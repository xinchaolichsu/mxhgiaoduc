<?php

use App\Entity\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_superadmin = Role::where('slug', 'superadmin')->first();
        $role_admin      = Role::where('slug', 'admin')->first();
        $role_manager    = Role::where('slug', 'manager')->first();
        $role_content    = Role::where('slug', 'content')->first();
        $role_user       = Role::where('slug', 'user')->first();

        $set_accounts = [
            [
                'email' => 'superadmin@gmail.com',
                'role'  => 'role_superadmin',
            ],
            [
                'email' => 'xinchaolichsu@gmail.com',
                'role'  => 'role_superadmin',
            ],
            [
                'email' => 'admin@gmail.com',
                'role'  => 'role_admin',
            ],
            [
                'email' => 'daudq.info@gmail.com',
                'role'  => 'role_admin',
            ],
            [
                'email' => 'manager@gmail.com',
                'role'  => 'role_manager',
            ],
            [
                'email' => 'content@gmail.com',
                'role'  => 'role_content',
            ],
            [
                'email' => 'user@gmail.com',
                'role'  => 'role_user',
            ],
        ];

        foreach ($set_accounts as $key => $set) {
            $faker = new Faker\Generator();
            $faker->addProvider(new Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new Faker\Provider\en_US\Address($faker));
            $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
            $faker->addProvider(new Faker\Provider\en_US\Company($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
            $faker->addProvider(new Faker\Provider\Internet($faker));

            $user         = new User();
            $user->gender = mt_rand(0, 1);
            if ($user->gender == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }
            $user->first_name         = $faker->firstName($gender);
            $user->last_name          = $faker->lastName($gender);
            $user->username           = $faker->email();
            $user->password           = Hash::make('default');
            $user->phone_number       = '1234567890';
            $user->birth              = '1993-01-05';
            $user->address            = $faker->address();
            $user->city               = $faker->city();
            $user->country            = $faker->country();
            $user->summary            = '';
            $user->email_verified     = 1;
            $user->is_updated_profile = 1;
            $user->active             = 1;
            $user->banned             = 0;
            $user->save();

            $user->attachRole(${$set['role']});
        }

        if (env('APP_ENV') != 'production') {
            $arr_username = [];
            for ($i = 0; $i < 100; $i++) {
                $faker = new Faker\Generator();
                $faker->addProvider(new Faker\Provider\en_US\Person($faker));
                $faker->addProvider(new Faker\Provider\en_US\Address($faker));
                $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
                $faker->addProvider(new Faker\Provider\en_US\Company($faker));
                $faker->addProvider(new Faker\Provider\Lorem($faker));
                $faker->addProvider(new Faker\Provider\Internet($faker));

                $username = $faker->email();
                if (in_array($username, $arr_username)) {
                    $username = $faker->email();
                }
                array_push($arr_username, $username);

                $user = new User();

                $user->gender = mt_rand(0, 1);
                if ($user->gender == 0) {
                    $gender = 'male';
                } else {
                    $gender = 'female';
                }
                $user->first_name         = $faker->firstName($gender);
                $user->last_name          = $faker->lastName($gender);
                $user->username           = $username;
                $user->password           = Hash::make('default');
                $user->phone_number       = '0123456789';
                $user->birth              = '1992-01-04';
                $user->address            = $faker->address();
                $user->city               = $faker->city();
                $user->country            = $faker->country();
                $user->summary            = '';
                $user->email_verified     = 1;
                $user->is_updated_profile = 1;
                $user->active             = 1;
                $user->banned             = 0;
                $user->save();

                $user->attachRole($role_user);
            }
        }
    }
}
