<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;
use NF\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();

        $superadmin = Role::Create(
        	[
                'name' => 'Super Admin',
        		'slug' => 'superadmin',
        	],
        	[
        		'name' => 'Super Admin',
        		'slug' => 'superadmin',
                'level' => 1
        	]
        );
        $superadmin->permissions()->sync($permissions->pluck('id'));

        /**
         * [$admin Role admin]
         * @var void
         */
        $admin = Role::updateOrCreate(
        	[
                'name' => 'Admin',
        		'slug' => 'admin',
        	],
        	[
        		'name' => 'Admin',
        		'slug' => 'admin',
                'level' => 2
        	]
        );
        $admin_permissions = $permissions->filter(function($item){
        	return $item->slug != 'create.admin' && $item->slug != 'update.admin' && $item->slug != 'delete.admin';
        });
        $admin->permissions()->sync($admin_permissions->pluck('id'));

        /**
         * [$manager Role manager]
         * @var void
         */
        $manager = Role::updateOrCreate(
        	[
                'name' => 'Manager',
        		'slug' => 'manager',
        	],
        	[
        		'name' => 'Manager',
        		'slug' => 'manager',
                'level' => 3
        	]
        );
        $manager_permissions = $permissions->filter(function($item){
        	return $item->slug != 'create.admin' && $item->slug != 'update.admin' && $item->slug != 'delete.admin' && $item->slug != 'delete.users';
        });
        $manager->permissions()->sync($manager_permissions->pluck('id'));

        /**
         * [$content Role content]
         * @var void
         */
        $content = Role::updateOrCreate(
        	[
                'name' => 'Content',
        		'slug' => 'content',
        	],
        	[
        		'name' => 'Content',
        		'slug' => 'content',
                'level' => 4
        	]
        );
        $content_permissions = $permissions->filter(function($item){
        	return $item->slug != 'view.roles' && $item->slug != 'create.roles' && $item->slug != 'update.roles' && $item->slug != 'delete.roles' && $item->slug != 'view.admin' && $item->slug != 'create.admin' && $item->slug != 'update.admin' && $item->slug != 'delete.admin' && $item->slug != 'view.users' && $item->slug != 'create.users' && $item->slug != 'update.users' && $item->slug != 'delete.users';
        });
        $content->permissions()->sync($content_permissions->pluck('id'));

        /**
         * [$user User]
         * @var [type]
         */
        $user = Role::updateOrCreate(
        	[
                'name' => 'User',
        		'slug' => 'user',
        	],
        	[
        		'name' => 'User',
        		'slug' => 'user',
                'level' => 5
        	]
        );
    }
}
