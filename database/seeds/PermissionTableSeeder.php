<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'View Roles',
                'slug' => 'view.roles',
                'description' => '',
            ],
            [
                'name' => 'Create Roles',
                'slug' => 'create.roles',
                'description' => '',
            ],
            [
                'name' => 'Update Roles',
                'slug' => 'update.roles',
                'description' => '',
            ],
            [
                'name' => 'Delete Roles',
                'slug' => 'delete.roles',
                'description' => '',
            ],
            [
                'name' => 'View Admin',
                'slug' => 'view.admin',
                'description' => '',
            ],
            [
                'name' => 'Create Admin',
                'slug' => 'create.admin',
                'description' => '',
            ],
            [
                'name' => 'Update Admin',
                'slug' => 'update.admin',
                'description' => '',
            ],
            [
                'name' => 'Delete Admin',
                'slug' => 'Delete.admin',
                'description' => '',
            ],
            [
                'name' => 'View Users',
                'slug' => 'view.users',
                'description' => '',
            ],
            [
                'name' => 'Create Users',
                'slug' => 'create.users',
                'description' => '',
            ],
            [
                'name' => 'Update Users',
                'slug' => 'update.users',
                'description' => '',
            ],
            [
                'name' => 'Delete Users',
                'slug' => 'Delete.users',
                'description' => '',
            ],
            [
                'name' => 'View Posts',
                'slug' => 'view.posts',
                'description' => '',
            ],
            [
                'name' => 'Create Posts',
                'slug' => 'create.posts',
                'description' => '',
            ],
            [
                'name' => 'Update Posts',
                'slug' => 'update.posts',
                'description' => '',
            ],
            [
                'name' => 'Delete Posts',
                'slug' => 'delete.posts',
                'description' => '',
            ],
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'slug' => $permission['slug'],
                'description' => $permission['description'],
            ]);
        }
    }
}
