<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function(Blueprint $table){
           $table->bigIncrements('ID');
           $table->bigInteger('post_author');
           $table->string('post_title');
           $table->text('post_content');
           $table->text('post_excerpt');
           $table->string('post_status');
           $table->string('post_name');
           $table->string('post_parent');
           $table->string('guid');
           $table->string('menu_order');
           $table->string('post_type');
           $table->string('comment_count');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
