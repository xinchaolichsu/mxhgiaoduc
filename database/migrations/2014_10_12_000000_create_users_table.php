<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('first_name', 20);
            $table->string('last_name', 20);
            $table->string('username', 100)->unique();
            $table->string('password');
            $table->string('phone_number', 25);
            $table->boolean('gender')->comment('0: male, 1: female');
            $table->date('birth', 20);
            $table->string('address');
            $table->string('city', 100);
            $table->string('country', 100);
            $table->text('summary');
            $table->boolean('email_verified');
            $table->boolean('is_updated_profile');
            $table->boolean('is_social_account')->default(0);
            $table->boolean('active');
            $table->boolean('banned');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
