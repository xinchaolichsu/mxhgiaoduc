
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

//=================== custom ======================
require('./app/jquery-migrate-1.1.1.min.js');
require('./app/jquery-ui-1.9.2.min.js');
require('./app/modernizr.min.js');
require('./app/jquery.bxSlider.min.js');
require('./app/jquery.slimscroll.js');
require('./app/jquery.cookie.js');
require('./app/custom.js');