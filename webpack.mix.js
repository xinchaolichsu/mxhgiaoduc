const { mix } = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.scripts([
    './bower_components/tether/dist/js/tether.min.js',
    './bower_components/jquery/dist/jquery.min.js',
    './bower_components/bootstrap/dist/js/bootstrap.min.js',
    
    './resources/assets/js/app/jquery-migrate-1.1.1.min.js',
    './resources/assets/js/app/jquery-ui-1.9.2.min.js',
    './resources/assets/js/app/modernizr.min.js',
    
    './bower_components/datatables.net/js/jquery.dataTables.min.js',
    
    './resources/assets/js/app/jquery.bxSlider.min.js',
    './resources/assets/js/app/jquery.slimscroll.js',
    './resources/assets/js/app/jquery.cookie.js',
    './resources/assets/js/app/jquery.uniform.min.js',
    './resources/assets/js/app/flot/jquery.flot.min.js',
    './resources/assets/js/app/flot/jquery.flot.resize.min.js',
    './resources/assets/js/app/bootstrap-timepicker.min.js',
    './resources/assets/js/app/responsive-tables.js',
    './resources/assets/js/app/fullcalendar.min.js',
    './resources/assets/js/app/custom.js',
], 'public/js/all.js');

// mix.js('resources/assets/js/app.js', 'public/js').sourceMaps();

mix.sass( 'resources/assets/sass/app_sass.scss' , 'public/css')
    .options({
        postCss: [
            require('postcss-css-variables')()
        ]
    })
    .sourceMaps();
mix.styles([
    // './bower_components/bootstrap/dist/css/bootstrap.min.css',
    './resources/assets/css/backend/app/bootstrap.min.css',
    './resources/assets/css/backend/app/bootstrap-responsive.min.css',
    './resources/assets/css/backend/app/jquery.ui.css',
    './resources/assets/css/backend/app/animate.min.css',
    './resources/assets/css/backend/app/animate.delay.css',
    './resources/assets/css/backend/app/isotope.css',
    './resources/assets/css/backend/app/colorbox.css',
    './resources/assets/css/backend/app/flexslider.css',
    './resources/assets/css/backend/app/uniform.tp.css',
    './resources/assets/css/backend/app/colorpicker.css',
    './resources/assets/css/backend/app/jquery.jgrowl.css',
    './resources/assets/css/backend/app/jquery.alerts.css',
    './resources/assets/css/backend/app/jquery.tagsinput.css',
    './resources/assets/css/backend/app/ui.spinner.css',
    './resources/assets/css/backend/app/jquery.chosen.css',
    './resources/assets/css/backend/app/fullcalendar.css',
    './resources/assets/css/backend/app/lato.css',
    './resources/assets/css/backend/app/font-awesome.min.css',
    './resources/assets/css/backend/app/bootstrap-timepicker.min.css',
    './resources/assets/css/backend/app/style.default.css',
    './resources/assets/css/backend/app/responsive-tables.css',
    './bower_components/datatables.net-dt/css/jquery.dataTables.min.css',
    './bower_components/font-awesome/css/font-awesome.min.css',
    './public/css/app_sass.css'
], 'public/css/all.css');
mix.browserSync({
    proxy: 'http://mxh-giaoduc.dev'
});